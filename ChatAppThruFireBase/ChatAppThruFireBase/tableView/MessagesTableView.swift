//
//  MessagesTableView.swift
//  ChatAppThruFireBase
//
//  Created by Naga Murala on 2/5/18.
//  Copyright © 2018 Naga Murala. All rights reserved.
//

import Foundation
import ProjectWorldFramework
import Firebase

class MessagesTableView: MasterTableView {
    let cellIdentifier = "messagesCell"
//    var cell: MessagesTableViewCell?
    var cell: UsersTableViewCell?
    
    override func initialize() {
        super.initialize()
        self.cellFactory.cellCreator = createTVC
        self.register(UsersTableViewCell.self, forCellReuseIdentifier: cellIdentifier)
    }
    
    func createTVC(item: Any, indexPath: IndexPath) -> UITableViewCell{
        cell = self.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? UsersTableViewCell
        guard let messagesCell = cell else { return UsersTableViewCell()}
        return messagesCell.configureCell(item: item, tableView: self, indexPath: indexPath)
    }
    
    //Overriding the following methods to display the delete button on left swipe of the table row
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    //This method will trigger when user presses on delete button in swipe gesture
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        //If user presses on delete button on any row in message table controller. we have to remove the Message toId from "user-messages".
        guard let message = self.list[indexPath.section]![indexPath.row] as? Message, let toID = message.toId else { return }
        guard let userID = Auth.auth().currentUser?.uid else { return }
        Database.database().reference().child("user_messages").child(userID).child(toID).removeValue { (error, dbReferance) in
            if error != nil {
                print("========Error while deleting the message row=======")
                print(error)
                print("===================================================")
            }
        }
        DispatchQueue.main.async {
            self.reloadData()
        }
        
    }
}
