//
//  ChatMessageCollectionView.swift
//  ChatAppThruFireBase
//
//  Created by Naga Murala on 2/13/18.
//  Copyright © 2018 Naga Murala. All rights reserved.
//

import UIKit
import Firebase
import ProjectWorldFramework

class ChatMessageCollectionView: MasterCollectionView {
    var chatMessageCell = "ChatMessageCollectionVC"
    var cell: ChatMessageCollectionViewCell?
    var itemHeight: Float?
    var receiver: User?
    init(frame: CGRect = .zero) {
        super.init(frame: frame, collectionViewLayout: UICollectionViewFlowLayout())
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //MARK:- Overridable flowlayout delegate methods
    open override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var height: CGFloat = 60
        if self.list.count > 0, indexPath.section >= 0, indexPath.row >= 0 {
            guard let item = self.list[indexPath.section]?[indexPath.row] else { return CGSize(width: self.frame.width, height: 40) }
            height = configureItemHeight(item: item, indexPath: indexPath) ?? 40.0
        }
        return CGSize(width: self.frame.width, height: height)
    }
    
    open override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    override func initialize() {
        self.register(ChatMessageCollectionViewCell.self, forCellWithReuseIdentifier: chatMessageCell)
        super.initialize()
        //To have some space between the table view top and first cell
        self.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 20, right: 0)
//        self.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
//        self.cellFactory.cellCreator
    }
    override func createCellConfiguration(item: Any, indexPath: IndexPath) -> UICollectionViewCell {
        cell = self.dequeueReusableCell(withReuseIdentifier: chatMessageCell, for: indexPath) as? ChatMessageCollectionViewCell
        guard let _cell = cell else { return ChatMessageCollectionViewCell() }
        cell = _cell.configureCell(collectionView: self, item: item, indexPath: indexPath) as? ChatMessageCollectionViewCell
        return cell ?? ChatMessageCollectionViewCell()
    }
    
    //Setting up dynamic height of the cell based upon the data
    func configureItemHeight(item: Any, indexPath: IndexPath) -> CGFloat? {
        guard let message = item as? Message else { return 40 }
        var dataText = ""
        if let dataItem = message.text {
            dataText = dataItem
        }
        else {
            return 300
        }
        //Get the estimated height of message text
        let approximateWidthOfMessageText = self.frame.width
        let size = CGSize(width: approximateWidthOfMessageText - (self.frame.width / 4) - 5 - 50, height: 1000)
        let attributes = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14.0)]
        let estimatedFrame = NSString(string: dataText).boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
        return estimatedFrame.height > 40 ? estimatedFrame.height:40
    }
    
    open var layout: UICollectionViewFlowLayout? {
        get {
            return collectionViewLayout as? UICollectionViewFlowLayout
        }
    }
}
