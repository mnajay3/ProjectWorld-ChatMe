//
//  ContactsTableView.swift
//  ChatAppThruFireBase
//
//  Created by Naga Murala on 1/23/18.
//  Copyright © 2018 Naga Murala. All rights reserved.
//

import UIKit
import ProjectWorldFramework

class ContactsTableView: MasterTableView {
    let cellIdentifier = "Cell"
    var reUsableCell: UsersTableViewCell?
    override func initialize() {
        super.initialize()
        self.cellFactory.cellCreator = cellCreator
        self.register(UsersTableViewCell.self, forCellReuseIdentifier: cellIdentifier)
    }
    
    func cellCreator(item: Any, indexPath:IndexPath) -> UITableViewCell {
        reUsableCell = self.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? UsersTableViewCell
        guard  let cell = reUsableCell else { return UsersTableViewCell() }
        return cell.configureCell(item: item, tableView: self, indexPath: indexPath)
    }
}
