//
//  LoginPageCell.swift
//  audible
//
//  Created by Naga Murala on 7/20/18.
//  Copyright © 2018 Naga Murala. All rights reserved.
//

import UIKit

//Protocol conforming to the class bound explicitly since, we are trying to create a weak referance for it and weak cannot be applied to enum types.
protocol LoginPageDelegate: class {
    func finishLogin(index: Int, cell: AnyObject)
    func handleTapOnProfileImage(_ cell: AnyObject)
}

class LoginPageCell: UICollectionViewCell, UITextFieldDelegate {
    
    weak var loginDelegate: LoginPageDelegate?
    
    lazy var imageView: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "Audible")
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleSelectProfileImage))
        iv.addGestureRecognizer(tapGesture)
        iv.isUserInteractionEnabled = true
        return iv
    }()
    
    @objc fileprivate func handleSelectProfileImage() {
        loginDelegate?.handleTapOnProfileImage(self)
    }
    
    lazy var loginRegisterSegmentControl: UISegmentedControl = {
        let sc = UISegmentedControl(items: ["Login", "Register"])
        sc.translatesAutoresizingMaskIntoConstraints = false
        sc.tintColor = .orange
        sc.selectedSegmentIndex = 0
        self.perform(#selector(handleSegmentControl), with: nil, afterDelay: 0)
        sc.addTarget(self, action: #selector(handleSegmentControl), for: .valueChanged)
        return sc
    }()
    
    //This method is responsible to hide and show the username field based on segment selection
    @objc fileprivate func handleSegmentControl() {
        let index = loginRegisterSegmentControl.selectedSegmentIndex
        //Set the button title based on segment selection
        let title = loginRegisterSegmentControl.titleForSegment(at: index)
        loginOrRegisterButton.setTitle(title, for: .normal)
        if index == 0 {
            userNameConstraints[3].constant = 0
            imageView.isUserInteractionEnabled = false
        } else {
            userNameConstraints[3].constant = 50
            imageView.isUserInteractionEnabled = true
        }
        UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.layoutIfNeeded()
        }, completion: nil)
    }
    
    var userName: PaddedTextField = {
        let tv = PaddedTextField()
        tv.placeholder = "User name"
        tv.layer.borderColor = UIColor.lightGray.cgColor
        tv.layer.borderWidth = 0.5
        return tv
    }()
    
    var emailTextField: PaddedTextField = {
        let tv = PaddedTextField()
        tv.placeholder = "Enter Email"
        tv.layer.borderColor = UIColor.lightGray.cgColor
        tv.layer.borderWidth = 0.5
        tv.keyboardType = .emailAddress
        return tv
    }()
    
    var passwordTextField: PaddedTextField = {
        let tv = PaddedTextField()
        tv.placeholder = "Enter Password"
        tv.isSecureTextEntry = true
        tv.layer.borderColor = UIColor.lightGray.cgColor
        tv.layer.borderWidth = 0.5
        return tv
    }()
    
    lazy var loginOrRegisterButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .orange
        button.setTitle("Login", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(loginOrRegisterPressed), for: .touchUpInside)
        return button
    }()
    
    @objc fileprivate func loginOrRegisterPressed() {
        loginDelegate?.finishLogin(index: loginRegisterSegmentControl.selectedSegmentIndex, cell: self)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupViews() {
        addSubview(imageView)
        addSubview(loginRegisterSegmentControl)
        addSubview(userName)
        addSubview(emailTextField)
        addSubview(passwordTextField)
        addSubview(loginOrRegisterButton)
        setupConstraints()
        emailTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    var userNameConstraints: [NSLayoutConstraint]!
    func setupConstraints() {
        _ = imageView.anchor(top: centerYAnchor, bottom: nil, left: nil, right: nil, topConstant: -300, bottomConstant: 0, leftConstant: 0, rightConstant: 0, height: 160, width: 160)
        imageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
    
        _ = loginRegisterSegmentControl.anchor(top: imageView.bottomAnchor, bottom: nil, left: leftAnchor, right: rightAnchor, topConstant: 8, bottomConstant: 0, leftConstant: 32, rightConstant: -32, height: 40, width: 0)
        
        userNameConstraints = userName.anchor(top: loginRegisterSegmentControl.bottomAnchor, bottom: nil, left: leftAnchor, right: rightAnchor, topConstant: 8, bottomConstant: 0, leftConstant: 32, rightConstant: -32, height: 50, width: 0)
        
        _ = emailTextField.anchor(top: userName.bottomAnchor, bottom: nil, left: leftAnchor, right: rightAnchor, topConstant: 0, bottomConstant: 0, leftConstant: 32, rightConstant: -32, height: 50, width: 0)
        
        _ = passwordTextField.anchor(top: emailTextField.bottomAnchor, bottom: nil, left: emailTextField.leftAnchor, right: emailTextField.rightAnchor, topConstant: 0, bottomConstant: 0, leftConstant: 0, rightConstant: 0, height: 50, width: 0)
        
        _ = loginOrRegisterButton.anchor(top: passwordTextField.bottomAnchor, bottom: nil, left: passwordTextField.leftAnchor, right: passwordTextField.rightAnchor, topConstant: 8, bottomConstant: 0, leftConstant: 0, rightConstant: 0, height: 50, width: 0)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.endEditing(true)
        }, completion: nil)
        return true
    }
}

class PaddedTextField: UITextField {
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10, y: bounds.origin.y, width: bounds.width + 10, height: bounds.height)
    }
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 10, y: bounds.origin.y, width: bounds.width + 10, height: bounds.height)
    }
}

