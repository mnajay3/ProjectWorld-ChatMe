//
//  PageCell.swift
//  audible
//
//  Created by Naga Murala on 7/17/18.
//  Copyright © 2018 Naga Murala. All rights reserved.
//

import UIKit

class PageCell: UICollectionViewCell {
    var leftRightConstraint: [NSLayoutConstraint]!
    var orientationChaged: Bool? {
        didSet{
            resetConstraints()
        }
    }
    let imageView: UIImageView = {
        let iv = UIImageView()
        //        iv.image = #imageLiteral(resourceName: "page1")
        return iv
    }()
    
    var page: Page? {
        didSet {
            guard let page = page else { return }
            imageView.image = page.image
            setAttributedText()
        }
    }
    let textView: UITextView = {
        let tv = UITextView()
        tv.isUserInteractionEnabled = false
        tv.contentInset = UIEdgeInsets(top: 24, left: 0, bottom: 0, right: 0)
        return tv
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupViews() {
        self.addSubview(imageView)
        self.addSubview(textView)
        //Set the constraints using UIView extensions method
        //        imageView.setConstraints(top: topAnchor, bottom: textView.topAnchor, left: leftAnchor, right: rightAnchor)
        leftRightConstraint = imageView.anchor(top: topAnchor, bottom: textView.topAnchor, left: leftAnchor, right: rightAnchor)
        textView.setConstraints(bottom: bottomAnchor, left: leftAnchor, right: rightAnchor)
        textView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.3).isActive = true
        setAttributedText()
    }
    
    func setAttributedText() {
        guard let page = page else { return }
        let color = UIColor(white: 0.2, alpha: 1)
        let attributedText = NSMutableAttributedString(string: page.title, attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 20.0), NSAttributedStringKey.foregroundColor: color])
        resetConstraints()
        attributedText.append(NSAttributedString(string: "\n\n\(page.message)", attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 14.0), NSAttributedStringKey.foregroundColor : color]))
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        let lenght = attributedText.string.count
        attributedText.addAttributes([NSAttributedStringKey.paragraphStyle : paragraphStyle], range: NSRange(location: 0, length: lenght))
        textView.attributedText = attributedText
    }
    
    func resetConstraints() {
        if UIDevice.current.orientation.isLandscape {
            leftRightConstraint[2].isActive = false
            leftRightConstraint[3].isActive = false
            leftRightConstraint = imageView.anchor(top: topAnchor, bottom: textView.topAnchor, left: leftAnchor, right: rightAnchor, topConstant: 0, bottomConstant: 0, leftConstant: 150, rightConstant: -150)
            leftRightConstraint[2].isActive = true
            leftRightConstraint[3].isActive = true
        } else {
            leftRightConstraint[2].isActive = false
            leftRightConstraint[3].isActive = false
            leftRightConstraint = imageView.anchor(top: topAnchor, bottom: textView.topAnchor, left: leftAnchor, right: rightAnchor, topConstant: 0, bottomConstant: 0, leftConstant: 0, rightConstant: 0)
            leftRightConstraint[2].isActive = true
            leftRightConstraint[3].isActive = true
        }
    }
}

