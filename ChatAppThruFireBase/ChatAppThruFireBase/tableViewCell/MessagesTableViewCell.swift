////
////  MessagesTableViewCell.swift
////  ChatAppThruFireBase
////
////  Created by Naga Murala on 2/5/18.
////  Copyright © 2018 Naga Murala. All rights reserved.
////
//
//import Foundation
//import ProjectWorldFramework
//import Firebase
//
//
////We are no longer using this class instead we are re-using the UsersTableViewCell class. Try changnoting UsersTableViewCell class name since it is no longer specific
//class MessagesTableViewCell: MasterTableViewCell {
////    var message: Message? {
////        didSet {
////            if let toId = message?.toId {
////                let ref = Database.database().reference().child("users").child(toId)
////                ref.observeSingleEvent(of: .value, with: { (snapShot) in
////                    if let dictionary = snapShot.value as? [String: Any] {
////                        let user = User(json: dictionary as [String : AnyObject])
////                        DispatchQueue.main.async {
////                            self.textLabel?.text = user.name
////                            self.detailTextLabel?.text = self.message?.text
////                        }
////                    }
////                }, withCancel: nil)
////            }
////        }
////    }
////
////    override func configureCell(item: Any, tableView: UITableView, indexPath: IndexPath, cell: UITableViewCell) -> UITableViewCell {
////        self.message = item as? Message
////        return self
////    }
//}
