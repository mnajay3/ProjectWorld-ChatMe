//
//  UsersTableViewCell.swift
//  ChatAppThruFireBase
//
//  Created by Naga Murala on 1/23/18.
//  Copyright © 2018 Naga Murala. All rights reserved.
//

import UIKit
import ProjectWorldFramework
import Firebase

class UsersTableViewCell: MasterTableViewCell {
    var message: Message? {
        didSet {
            let currentUser = Auth.auth().currentUser?.uid
            var id : String?
            if currentUser == message?.fromId {
                id = message?.toId
            } else {
                id = message?.fromId
            }
            if let userID = id {
                let ref = Database.database().reference().child("users").child(userID)
                ref.observeSingleEvent(of: .value, with: { (snapShot) in
                    let toIdKey = snapShot.key
                    if let dictionary = snapShot.value as? [String: Any] {
                        let user = User(json: dictionary as [String : AnyObject], withKey: toIdKey)
                        DispatchQueue.main.async {
                            self.userNameLabel.text = user.name
                            self.emailLabel.text = self.message?.text
                            if let urlString = user.profileImageURL {
                                self.profileImageView.loadImageUsingCacheWithUrlString(urlString: urlString)
                            }
                            
                            self.message?.toUser = user
                            guard let timeStamp = self.message?.timestamp?.doubleValue else { return }
                            let timeStampValue = NSDate(timeIntervalSince1970: timeStamp)
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "hh:mm:ss a"
                            let date = dateFormatter.string(from: timeStampValue as Date)
                            self.timeLabel.text = date
                        }
                    }
                }, withCancel: nil)
            }
        }
    }
    
    var profileImageView: UIImageView = {
        let pi = UIImageView()
        pi.translatesAutoresizingMaskIntoConstraints = false
        pi.layer.cornerRadius = 25
        pi.clipsToBounds = true
        pi.contentMode  = .scaleAspectFill
        pi.image = #imageLiteral(resourceName: "profileImg")
        return pi
    }()
    var userNameLabel: UILabel = {
        let unl = UILabel()
        unl.translatesAutoresizingMaskIntoConstraints = false
        unl.font = UIFont(name: "HelveticaNeue-Bold", size: 16)
        return unl
    }()
    var emailLabel: UILabel = {
        let el = UILabel()
        el.translatesAutoresizingMaskIntoConstraints = false
        el.font = UIFont(name: "HelveticaNeue-Bold", size: 14)
        el.textColor = .lightGray
        return el
    }()
    var timeLabel: UILabel = {
        let tl = UILabel()
        tl.translatesAutoresizingMaskIntoConstraints = false
        tl.font = UIFont(name: "HelveticaNeue", size: 14)
        tl.textColor = .lightGray
        tl.text = "HH:MM:SS"
        tl.isHidden = true
        return tl
    }()
    
    //PRAGMARK:- Initialization Methods
    //Make sure to override the init, since you are customizing without storyboards and specifc style
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        initialize()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    func initialize() {
        addSubview(profileImageView)
        addSubview(userNameLabel)
        addSubview(emailLabel)
        addSubview(timeLabel)
        addConstraints()
    }
    
    //PRAGMARK:- PROJECTFRAMEWORK overriden methods
    override func configureCell(item: Any, tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        //Cell information for ContactsViewController
        if let item = item as? User {
            self.userNameLabel.text = item.name ?? "Not available"
            self.emailLabel.text = item.email ?? "Not available"
            self.timeLabel.isHidden = true
            if let urlString = item.profileImageURL {
                profileImageView.loadImageUsingCacheWithUrlString(urlString: urlString)
            }
        }
            //Cell information for MessageViewController
        else if let _message = item as? Message {
            self.message = _message
            self.timeLabel.isHidden = false
        }
        
        return self
    }
    
    //PRAGMARK:- Add Constraints to UIVIEWS
    func addConstraints() {
        profileImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        profileImageView.widthAnchor.constraint(equalToConstant: frame.height + 10).isActive = true
        profileImageView.heightAnchor.constraint(equalToConstant: frame.height + 10).isActive = true
        profileImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        
        userNameLabel.topAnchor.constraint(equalTo: profileImageView.topAnchor, constant: 0).isActive = true
        userNameLabel.leftAnchor.constraint(equalTo: profileImageView.rightAnchor, constant: 10).isActive = true
        userNameLabel.heightAnchor.constraint(equalToConstant: frame.height / 2).isActive = true
        userNameLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        
        emailLabel.topAnchor.constraint(equalTo: userNameLabel.bottomAnchor, constant: 0).isActive = true
        emailLabel.leftAnchor.constraint(equalTo: userNameLabel.leftAnchor, constant: 0).isActive = true
        emailLabel.heightAnchor.constraint(equalToConstant: frame.height / 2).isActive = true
        emailLabel.rightAnchor.constraint(equalTo: userNameLabel.rightAnchor).isActive = true
        
        timeLabel.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        timeLabel.centerYAnchor.constraint(equalTo: userNameLabel.centerYAnchor).isActive = true
        timeLabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        timeLabel.heightAnchor.constraint(equalTo: userNameLabel.heightAnchor).isActive = true
        
    }
}
