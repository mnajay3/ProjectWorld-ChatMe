//
//  ChatMessageCollectionViewCell.swift
//  ChatAppThruFireBase
//
//  Created by Naga Murala on 2/13/18.
//  Copyright © 2018 Naga Murala. All rights reserved.
//
/**
 
 It is responsible to display the chat (messages, images, videos) in chat box to a specific receiver
 
 **/

import UIKit
import ProjectWorldFramework
import Firebase
import AVFoundation

class ChatMessageCollectionViewCell: MasterCollectionViewCell {
    var chatBubbleView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 16
        view.layer.masksToBounds = true
        return view
    }()
    
    var messageText : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 50
        label.font = UIFont.systemFont(ofSize: 14.0)
//        label.backgroundColor = .green
        return label
    }()
    
    lazy var imageView: UIImageView = {
       let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
//        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var playButton: UIButton = {
        let pb = UIButton(type: .system)
        pb.translatesAutoresizingMaskIntoConstraints = false
        pb.setImage(#imageLiteral(resourceName: "play"), for: .normal)
        pb.tintColor = .white
        pb.addTarget(self, action: #selector(handlePlayButton), for: .touchUpInside)
        return pb
    }()
    lazy var activityIndicator: UIActivityIndicatorView = {
        let aiv = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        aiv.translatesAutoresizingMaskIntoConstraints = false
        aiv.hidesWhenStopped = true
        return aiv
    }()
    
    override func initialize() {
        super.initialize()
        self.addSubview(chatBubbleView)
        chatBubbleView.addSubview(messageText)
        addConstraints()
    }
    
    var message: Message?
    override func configureCell(collectionView: UICollectionView, item: Any, indexPath: IndexPath) -> UICollectionViewCell {
        message = item as? Message
        //Calling method, which is implemented in ChatMessageCVCell extension
        setMessages(item: item, collectionView: collectionView)
        return self
    }
    
    //Life cycle method, gets invoked everytime when the cell is getting reused
    override func prepareForReuse() {
        super.prepareForReuse()
        self.imageView.removeFromSuperview()
        self.playButton.removeFromSuperview()
        self.player?.pause()
        self.playerLayer?.removeFromSuperlayer()
    }
    
    var player: AVPlayer?
    var playerLayer: AVPlayerLayer?
    @objc func handlePlayButton()  {
        guard let videoUrlString = message?.videoURL, let videoUrl = URL(string: videoUrlString) else {
            print("====Video URL is empty====")
            return
        }
        player = AVPlayer(url: videoUrl)
        playerLayer = AVPlayerLayer(player: player)
        playerLayer?.frame = chatBubbleView.bounds
        chatBubbleView.layer.addSublayer(playerLayer!)
        player?.play()
        activityIndicator.startAnimating()
        print("Play button pressed", message?.videoURL)
    }
    
    var messageTextAlignAnchor : NSLayoutConstraint?
    var chatBubbleAlignAnchor : NSLayoutConstraint?
    var chatBubbleWidthAnchor: NSLayoutConstraint?
    var imageViewAlignAnchor: NSLayoutConstraint?
}
