//
//  Message.swift
//  ChatAppThruFireBase
//
//  Created by Naga Murala on 2/5/18.
//  Copyright © 2018 Naga Murala. All rights reserved.
//

import Foundation
import ProjectWorldFramework

class Message: NSObject {
    var toId: String?
    var timestamp: NSNumber?
    var text: String?
    var fromId: String?
    var imageURL: String?
    var videoURL: String?
    //Declaring as an alternative to get the to user information
    var toUser: User?
    
    init(json: JSON) {
        self.toId = json["toId"] as? String
        self.timestamp = json["timestamp"] as? NSNumber
        self.text = json["text"] as? String
        self.fromId = json["fromId"] as? String
        self.imageURL = json["imageURL"] as? String
        self.videoURL = json["videoURL"] as? String
    }
}
