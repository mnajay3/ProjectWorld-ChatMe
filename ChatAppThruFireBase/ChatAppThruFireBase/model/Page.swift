//
//  Page.swift
//  audible
//
//  Created by Naga Murala on 7/17/18.
//  Copyright © 2018 Naga Murala. All rights reserved.
//

import UIKit

struct Page {
    var title: String
    var message: String
    var image: UIImage
}
