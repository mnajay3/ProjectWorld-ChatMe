//
//  User.swift
//  ChatAppThruFireBase
//
//  Created by Naga Murala on 1/23/18.
//  Copyright © 2018 Naga Murala. All rights reserved.
//

import UIKit

class User: NSObject {
    //user id is to get the User uid information, it's ot the actual part of User
    var userID: String?
    var name: String?
    var email: String?
    var profileImageURL: String?
    
    init(json: [String: AnyObject], withKey key: String? = nil) {
        self.userID = key ?? nil
        self.name = json["name"] as? String
        self.email = json["email"] as? String
        self.profileImageURL = json["profileImageURL"] as? String
    }
}
