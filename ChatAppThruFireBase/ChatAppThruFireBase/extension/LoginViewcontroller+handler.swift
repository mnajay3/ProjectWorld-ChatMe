//
//  LoginViewController+handlers.swift
//  ChatAppThruFireBase
//
//  Created by Naga Murala on 1/27/18.
//  Copyright © 2018 Naga Murala. All rights reserved.
//

import UIKit
import Firebase

extension LoginViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @objc func handleSelectProfileImage() {
        let picker = UIImagePickerController()
        //Assign delegate to self to override picker delegate methods
        picker.delegate = self
        //This will allow member to edit the selected image in camara role
        picker.allowsEditing = true
        present(picker, animated: true, completion: nil)
    }
    
    func handleRegeistration(_ cell: AnyObject) {
        guard let lpCell = cell as? LoginPageCell else { return }
        guard let email = lpCell.emailTextField.text,let password = lpCell.passwordTextField.text else { return }
        let strSelf = self
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            if error != nil {
                print("Something went wrong with fire base email Authentication:", error?.localizedDescription as Any)
                return
            }
            DispatchQueue.main.async {
                guard let user = user else { return }
                strSelf.uploadImagedataToStorage(lpCell, user: user)
            }
        }
        
    }
    func uploadImagedataToStorage(_ cell: LoginPageCell, user: Firebase.User) {
        guard let email = cell.emailTextField.text, let name = cell.userName.text else { return }
        let imageName = NSUUID().uuidString
        //Upload the image into storage
        let storageRef = Storage.storage().reference().child("profile_images").child("\(imageName).jpg")
        //            if let uploadData = UIImagePNGRepresentation(self.profileImageView.image!)
        //Becareful with force unwrapping. Try safely unwrap it in future
        if let uploadData = UIImageJPEGRepresentation(cell.imageView.image!, 0.1)
        {
            let strRef = self
            storageRef.putData(uploadData, metadata: nil, completion: { (metaData, error) in
                if error != nil {
                    print(error ?? "Error while insertng image into Storage")
                    return
                }
                //Fetch the image uploaded url from storage and pass it to database to store along with user input values.
                if let profileImageUrl = metaData?.downloadURL()?.absoluteString {
                    DispatchQueue.main.async {
                        let userValues = ["email":email, "name":name, "profileImageURL": profileImageUrl]
                        strRef.registerUserIntoFireBaseWithUID(uid: user.uid, values: userValues)
                    }
                }
            })
        }
    }
    
    func handleLogin(_ cell: AnyObject) {
        guard let lpCell = cell as? LoginPageCell else { return }
        guard let email = lpCell.emailTextField.text, let password = lpCell.passwordTextField.text else { return }
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if error != nil  {
                print("Unable to sign in with the credentials:", error?.localizedDescription ?? "nil")
                let alert = UIAlertController(title: "Login Failed", message: "Invalid credentials, Please check and try login again", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
            print("User signed in successfully")
            NotificationCenter.default.post(name: NSNotification.Name("Notification_load_CurrenUser"), object: nil, userInfo: nil)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func registerUserIntoFireBaseWithUID(uid: String, values: [String: String]) {
        let ref = Database.database().reference(fromURL: "https://chatappthrufirebase.firebaseio.com/")
        let mainChild = ref.child("users")
        let userChild = mainChild.child(uid)
        let strRef = self
        userChild.updateChildValues(values, withCompletionBlock: { (error, ref) in
            if error != nil {
                print("Something went wront while inserting values into Firebase:", error?.localizedDescription ?? "Error is nil")
                return
            }
            print("Firebase has been updated successfully")
            DispatchQueue.main.async {
                let user = User(json: values as [String : AnyObject])
                let userDict = ["user" : user]
                NotificationCenter.default.post(name: NSNotification.Name("Notification_load_CurrenUser"), object: nil, userInfo: userDict)
                strRef.dismiss(animated: true, completion: nil)
            }
        })
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let lpCell = self.cell as? LoginPageCell else { return }
        if let cropImage = info["UIImagePickerControllerEditedImage"] {
            lpCell.imageView.image = cropImage as? UIImage
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] {
            lpCell.imageView.image = originalImage as? UIImage
        }
        dismiss(animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
