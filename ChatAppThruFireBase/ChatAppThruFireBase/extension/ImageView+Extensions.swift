//
//  Extensions.swift
//  ChatAppThruFireBase
//
//  Created by Naga Murala on 1/29/18.
//  Copyright © 2018 Naga Murala. All rights reserved.
//

import UIKit

var imageCache  = NSCache<AnyObject, AnyObject>()
extension UIImageView {
    
    
    func loadImageUsingCacheWithUrlString(urlString: String) {
        self.image = nil
        if let cachedImage = imageCache.object(forKey: urlString as AnyObject) {
            self.image = cachedImage as? UIImage
            return
        }
        if let url = URL(string: urlString) {
            URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                if error != nil {
                    print("Something is wrong while downloading the image")
                    return
                }
                guard let data = data else {
                    print("Something went wrong in data")
                    return
                }
                DispatchQueue.main.async {
                    guard let downloadedImage = UIImage(data: data)
                        else {
                            return
                    }
                    imageCache.setObject(downloadedImage, forKey: urlString as AnyObject)
                    self.image = downloadedImage
                }
            }).resume()
        }
    }
    
}
