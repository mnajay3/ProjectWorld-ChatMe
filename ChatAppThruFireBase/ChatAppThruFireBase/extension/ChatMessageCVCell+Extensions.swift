//
//  ChatMessageCVCell+Extensions.swift
//  ChatAppThruFireBase
//
//  Created by Naga Murala on 5/3/18.
//  Copyright © 2018 Naga Murala. All rights reserved.
//

import Foundation
import ProjectWorldFramework
import Firebase

extension ChatMessageCollectionViewCell {
    //It handles the input item and sets the message in correct direction
    //It is so ugly, has to re-write whole logic
    func setMessages(item : Any, collectionView: UICollectionView) {
        guard let message = item as? Message else { return }
        guard  let senderID = message.fromId else { return }
        guard  let receiverID = message.toId else { return }
        guard let  uid = Auth.auth().currentUser?.uid else { return }
        switch uid {
        case senderID:
            if let imageURL = message.imageURL {
                self.messageText.isHidden = true
                self.imageView.isHidden = false
                self.imageView.loadImageUsingCacheWithUrlString(urlString: imageURL)
                self.chatBubbleView.addSubview(imageView)
                let constraint = self.imageView.rightAnchor.constraint(equalTo: self.chatBubbleView.rightAnchor)
                addImageViewConstraints(alignConstraint: constraint)
                if message.videoURL != nil {
                    self.chatBubbleView.addSubview(playButton)
                    addPlayButtonConstraints()
                }
            } else {
                self.imageView.isHidden = true
                self.messageText.isHidden = false
                self.messageText.text = message.text
                chatBubbleWidthAnchor?.isActive = false
                chatBubbleWidthAnchor = chatBubbleView.widthAnchor.constraint(equalTo: self.messageText.widthAnchor, constant: 20)
                chatBubbleWidthAnchor?.isActive = true
                self.messageText.layer.masksToBounds = true
                self.messageText.layer.cornerRadius = 5.0
                messageTextAlignAnchor?.isActive = false
                messageTextAlignAnchor = messageText.rightAnchor.constraint(equalTo: self.chatBubbleView.rightAnchor, constant: -10)
                messageTextAlignAnchor?.isActive = true
            }
            chatBubbleAlignAnchor?.isActive = false
            chatBubbleAlignAnchor = chatBubbleView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -5)
            self.chatBubbleView.backgroundColor =  UIColor(red: 0.93, green: 0.92, blue: 0.92, alpha: 1.0)
            chatBubbleAlignAnchor?.isActive = true
            
        case receiverID:
            if let imageURL = message.imageURL {
                self.messageText.isHidden = true
                self.imageView.isHidden = false
                self.imageView.loadImageUsingCacheWithUrlString(urlString: imageURL)
                self.chatBubbleView.addSubview(imageView)
                let constraint = self.imageView.leftAnchor.constraint(equalTo: self.chatBubbleView.leftAnchor)
                addImageViewConstraints(alignConstraint: constraint)
                if message.videoURL != nil {
                    self.chatBubbleView.addSubview(playButton)
                    addPlayButtonConstraints()
                }
            } else {
                self.messageText.isHidden = false
                self.imageView.isHidden = true
                self.messageText.text = message.text
                self.messageText.layer.masksToBounds = true
                self.messageText.layer.cornerRadius = 5.0
                messageTextAlignAnchor?.isActive = false
                messageTextAlignAnchor = messageText.leftAnchor.constraint(equalTo: self.chatBubbleView.leftAnchor, constant: 10)
                messageTextAlignAnchor?.isActive = true
                
                chatBubbleWidthAnchor?.isActive = false
                chatBubbleWidthAnchor = self.chatBubbleView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 3/4)
                chatBubbleWidthAnchor?.isActive = true
            }
            
            
            chatBubbleAlignAnchor?.isActive = false
            chatBubbleAlignAnchor = chatBubbleView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 5)
            chatBubbleView.backgroundColor = UIColor(red: 0.78, green: 0.80, blue: 0.82, alpha: 1.0)
            chatBubbleAlignAnchor?.isActive = true
        default:
            break
        }
        //item should be the message btwn current sender and receiver but have to decide the order
    }
    
    func addConstraints() {
        
        chatBubbleAlignAnchor = chatBubbleView.rightAnchor.constraint(equalTo: self.rightAnchor)
        chatBubbleAlignAnchor?.isActive = true
        chatBubbleView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        chatBubbleView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        chatBubbleWidthAnchor = chatBubbleView.widthAnchor.constraint(equalTo: self.messageText.widthAnchor, constant: 20)
        chatBubbleWidthAnchor?.isActive = true
        
        messageTextAlignAnchor = messageText.rightAnchor.constraint(equalTo: self.rightAnchor)
        messageTextAlignAnchor?.isActive = true
        messageText.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        messageText.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        messageText.widthAnchor.constraint(lessThanOrEqualToConstant: self.frame.width * (3/4)).isActive = true
    }
    /*
     func addConstraints(chatBubbleAlign:NSLayoutConstraint? = nil, chatBubbleWidth:NSLayoutConstraint? = nil, messageLabelAlign:NSLayoutConstraint? = nil) {
     chatBubbleAlignAnchor?.isActive = false
     chatBubbleAlignAnchor = chatBubbleAlign ?? chatBubbleView.rightAnchor.constraint(equalTo: self.rightAnchor)
     chatBubbleAlignAnchor?.isActive = true
     chatBubbleView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
     chatBubbleView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
     chatBubbleWidthAnchor?.isActive = false
     chatBubbleWidthAnchor = chatBubbleWidth ?? chatBubbleView.widthAnchor.constraint(equalTo: self.messageText.widthAnchor, constant: 20)
     chatBubbleWidthAnchor?.isActive = true
     
     
     messageTextAlignAnchor?.isActive = false
     messageTextAlignAnchor = messageLabelAlign ?? messageText.rightAnchor.constraint(equalTo: self.rightAnchor)
     messageTextAlignAnchor?.isActive = true
     messageText.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
     messageText.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
     messageText.widthAnchor.constraint(lessThanOrEqualToConstant: self.frame.width * (3/4)).isActive = true
     }
 
 
 */
    func addImageViewConstraints(alignConstraint: NSLayoutConstraint) {
        self.imageView.widthAnchor.constraint(equalTo: self.chatBubbleView.widthAnchor).isActive = true
        self.imageView.topAnchor.constraint(equalTo: self.chatBubbleView.topAnchor).isActive = true
        self.imageView.bottomAnchor.constraint(equalTo: self.chatBubbleView.bottomAnchor).isActive = true
        imageViewAlignAnchor = alignConstraint
        imageViewAlignAnchor?.isActive = true
    }
    
    func addPlayButtonConstraints() {
        self.playButton.centerXAnchor.constraint(equalTo: self.chatBubbleView.centerXAnchor).isActive = true
        self.playButton.centerYAnchor.constraint(equalTo: self.chatBubbleView.centerYAnchor).isActive = true
        self.playButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.playButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        self.chatBubbleView.addSubview(self.activityIndicator)
        self.activityIndicator.centerXAnchor.constraint(equalTo: self.chatBubbleView.centerXAnchor).isActive = true
        self.activityIndicator.centerYAnchor.constraint(equalTo: self.chatBubbleView.centerYAnchor).isActive = true
        self.activityIndicator.heightAnchor.constraint(equalToConstant: 50).isActive = true
        self.activityIndicator.widthAnchor.constraint(equalToConstant: 50).isActive = true
    }
}
