//
//  extensions+UIView.swift
//  audible
//
//  Created by Naga Murala on 7/17/18.
//  Copyright © 2018 Naga Murala. All rights reserved.
//

import UIKit

extension UIView {
    
    func setConstraints(top: NSLayoutYAxisAnchor? = nil, bottom: NSLayoutYAxisAnchor? = nil,
                        left: NSLayoutXAxisAnchor? = nil, right: NSLayoutXAxisAnchor? = nil) {
        
        setConstraintsWithConstants(top: top, bottom: bottom, left: left, right: right)
        
    }
    
    func setConstraintsWithConstants(top: NSLayoutYAxisAnchor? = nil,  bottom: NSLayoutYAxisAnchor? = nil,
                                     left: NSLayoutXAxisAnchor? = nil, right: NSLayoutXAxisAnchor? = nil,
                                     topConstant: CGFloat = 0, bottomConstant: CGFloat = 0,
                                     leftConstant: CGFloat = 0, rightConstant: CGFloat = 0) {
        
        _ = anchor(top: top, bottom: bottom, left: left, right: right, topConstant: topConstant, bottomConstant: bottomConstant, leftConstant: leftConstant, rightConstant: rightConstant)
        
    }
    
    func anchor(top: NSLayoutYAxisAnchor? = nil,  bottom: NSLayoutYAxisAnchor? = nil,
                left: NSLayoutXAxisAnchor? = nil, right: NSLayoutXAxisAnchor? = nil,
                topConstant: CGFloat = 0, bottomConstant: CGFloat = 0,
                leftConstant: CGFloat = 0, rightConstant: CGFloat = 0, height: CGFloat = 0,width: CGFloat = 0) -> [NSLayoutConstraint] {
        
        var anchors = [NSLayoutConstraint]()
        //Setting the custom view auto resizing constraints as false
        translatesAutoresizingMaskIntoConstraints = false
        if let top = top {
            anchors.append(topAnchor.constraint(equalTo: top, constant: topConstant))
        }
        if let bottom = bottom {
            anchors.append(bottomAnchor.constraint(equalTo: bottom, constant: bottomConstant))
        }
        if let left = left {
            anchors.append(leftAnchor.constraint(equalTo: left, constant: leftConstant))
        }
        if let right = right {
            anchors.append(rightAnchor.constraint(equalTo: right, constant: rightConstant))
        }
        if height > 0 {
            anchors.append(heightAnchor.constraint(equalToConstant: height))
        }
        if width > 0 {
            anchors.append(widthAnchor.constraint(equalToConstant: width))
        }
        anchors.forEach{$0.isActive = true}
        return anchors
    }
    
    func anchorWithMultiplier(height: NSLayoutDimension? = nil,  width: NSLayoutDimension? = nil,
                heightMultiplier: CGFloat = 1, widthMultiplier: CGFloat = 1) -> [NSLayoutConstraint] {
        
        var anchors = [NSLayoutConstraint]()
        //Setting the custom view auto resizing constraints as false
        translatesAutoresizingMaskIntoConstraints = false
        
        if let height = height {
            anchors.append(heightAnchor.constraint(equalTo: height, multiplier: heightMultiplier))
        }
        if let width = width {
            anchors.append(widthAnchor.constraint(equalTo: width, multiplier: widthMultiplier))
        }
        anchors.forEach{$0.isActive = true}
        return anchors
    }
}

