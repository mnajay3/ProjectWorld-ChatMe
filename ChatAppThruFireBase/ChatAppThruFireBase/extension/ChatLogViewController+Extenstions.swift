//
//  ChatLogViewController+Constraints.swift
//  ChatAppThruFireBase
//
//  Created by Naga Murala on 5/1/18.
//  Copyright © 2018 Naga Murala. All rights reserved.
//

import UIKit
import Firebase
import ProjectWorldFramework
import MobileCoreServices
import AVFoundation
/*************************************************************************************************
 *
 *
 *
 ************************************************************************************************/
//PRAGMA MARK:- Service Call extension
extension ChatLogViewController {
    @objc func handleSendEvent() {
        //How it works: Message will be stored in Messages object in firedatabase
        //Corresponding message ID will be stored under the current userID and recipient user id to show this
        //message in inbox for sender and recipient only
        guard let inputText = textField.text else { return }
        textField.resignFirstResponder()
        let ref = Database.database().reference().child("messages")
        let childRef = ref.childByAutoId()
        guard let fromId = Auth.auth().currentUser?.uid else {
            print("UID is nil in handleSendEvent()")
            return
        }
        guard let receiverID = user?.userID else {
            print("Receiver id is nil in handleSendEvent()")
            return
        }
        let timestamp: NSNumber = NSNumber(value: Date().timeIntervalSince1970)
        let values = ["fromId": fromId, "toId": receiverID, "text" : inputText, "timestamp": timestamp] as [String : Any]
        childRef.updateChildValues(values) { (error, ref) in
            if error != nil {
                print("errror occured while sending the message in handleSendEvent() ", error ?? "Error body is NA")
                return
            }
            let userMessagesRef = Database.database().reference().child("user_messages")
            //Update message under from user . toUser
            let fromRef = userMessagesRef.child(fromId).child(receiverID)
            fromRef.updateChildValues([childRef.key:1])
            if let toUID = self.user?.userID {
                let toRef = userMessagesRef.child(toUID).child(fromId)
                //Update the message under to user
                toRef.updateChildValues([childRef.key:1])
            }
        }
        textField.text = .none
    }
    
    /**
 Responsible for fetching all the messages under the sender(current user) for the receiver id.
 
 **/
    func getAllMessages() {
        let senderID = Auth.auth().currentUser?.uid
        guard let receiverID = user?.userID, let fromId = senderID else { return }
        let receiverMsgsRef = Database.database().reference().child("user_messages").child(fromId).child(receiverID)
        receiverMsgsRef.observe(.childAdded, with: { (snapShot) in
            print(snapShot.key)
            let messageID = snapShot.key
            let receiverMsgs = Database.database().reference().child("messages").child(messageID)
            receiverMsgs.observe(.value, with: { (snapShot) in
                guard let messageDict = snapShot.value as? JSON else { return }
                let message = Message(json: messageDict)
                //The following if condition is not really needed since all the message under receiverMsgsRef should obey the following condition
                if (senderID == message.fromId || senderID == message.toId),
                    (receiverID == message.fromId || receiverID == message.toId) {
                    self.messages.append(message)
                    DispatchQueue.main.asyncAfter(deadline: (.now() + 0.5), execute: {
                        self.messageCollectionView.list = [0: self.messages]
                        //Setting the current receiver to the table view to set the messages in order in chat view controller
                        self.messageCollectionView.receiver = self.user
                        self.messageCollectionView.reloadData()
                    })
                }
            }, withCancel: nil)
        }, withCancel: nil)
        
    }
    
   
}

/*************************************************************************************************
 *
 *
 *
 ************************************************************************************************/
//PRAGMA MARK:- TextField extension
extension ChatLogViewController: UITextFieldDelegate {
    //Input accessory view methods
    public override var inputAccessoryView: UIView? {
        get {
            return containerView
        }
    }
    
    public override var canBecomeFirstResponder: Bool {
        get {
            return true
        }
    }
    
    //TEXT FIELD Delegate methods and instance methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.textField.resignFirstResponder()
        return true
    }
    
    @objc func adjustHeightWithKeyBoard(notification: Notification) {
        //Don't have to set these constraints, if we us inputAccessoryView. It will take care of keyboar show and hide functionality
                guard let keyBoardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        //        containerViewBottomConstraint?.isActive = false
        //        containerViewBottomConstraint?.constant = -keyBoardSize.height
        //        containerViewBottomConstraint?.isActive = true
        let height = -keyBoardSize.height
        guard let keyBoardanimationDuration = (notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? Double) else { return }
        UIView.animate(withDuration: keyBoardanimationDuration, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.messageCVBottomConstraint?.isActive = false
            self.messageCVBottomConstraint?.constant = height
            self.messageCVBottomConstraint?.isActive = true
            self.messageCollectionView.layout?.invalidateLayout()
            self.view.layoutIfNeeded()
        }, completion: nil)
        
    }
    
    @objc func adjustHeightWithOutKeyBoard(notification: Notification) {
        guard let keyBoardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        //Don't have to set these constraints, if we us inputAccessoryView. It will take care of keyboar show and hide functionality
        //        containerViewBottomConstraint?.isActive = false
        //        containerViewBottomConstraint?.constant = 0
        //        containerViewBottomConstraint?.isActive = true
        let height:CGFloat = -50
        
        guard let keyBoardanimationDuration:Double = (notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? Double) else { return }
        UIView.animate(withDuration: keyBoardanimationDuration, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.messageCVBottomConstraint?.isActive = false
            self.messageCVBottomConstraint?.constant = height
            self.messageCVBottomConstraint?.isActive = true
            self.messageCollectionView.layout?.invalidateLayout()
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
}

/*************************************************************************************************
 * Image picker/ Video picker
 *
 *
 ************************************************************************************************/
//PRAGMA MARK:- Image picker view extension

extension ChatLogViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //Onclic ImagePicker Selector in containerView
    @objc func handleUploadImageViewEvent() {
        self.textField.resignFirstResponder()
        let imagePickerVC = UIImagePickerController()
        imagePickerVC.delegate = self
        imagePickerVC.allowsEditing = true
        imagePickerVC.mediaTypes = [kUTTypeImage,kUTTypeMovie] as [String]  //Mandetory to show videos options
        present(imagePickerVC, animated: true) {
            print("Image picker controller is opened")
        }
    }
    
    //Image picker delegation methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let videoUrl = info[UIImagePickerControllerMediaURL] as? URL {
            handleSelectedVideoForURL(videoUrl)
        } else {
            var image_: UIImage?
            if let cropImage = info["UIImagePickerControllerEditedImage"] {
                image_ = cropImage as? UIImage
            } else if let originalImage = info["UIImagePickerControllerOriginalImage"] {
                image_ = originalImage as? UIImage
            }
            guard let image = image_ else {
                self.dismiss(animated: true, completion: nil)
                return }
            storeImageInUserImages(image: image) { (imageUrl) in
                self.setImagePropertiesInMessage(imageUrl: imageUrl)
            }
        }
        
        //Getting the main thread
        DispatchQueue.main.async {
            self.messageCollectionView.layout?.invalidateLayout()
            self.dismiss(animated: true) {
                print("Image picked and closing the image picker controller")
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    //upload video url into firebase
    private func handleSelectedVideoForURL(_ url : URL) {
        let fileName = NSUUID().uuidString + ".mov"
        let videoUploadTask = Storage.storage().reference().child("message_movies").child(fileName).putFile(from: url, metadata: nil) { (metaData, err) in
            if err != nil {
                print("=============**ERROR**=================")
                print("Error occured while uploading the image into Firebase")
                print(err)
                print("=============**ERROR**=================")
                return
            }
            guard let thumbnailImage = self.thumbnailImageForVideoFileUrl(url) else {
                print("Unable to get the thumbnailImage for video to store in user images")
                return }
            if let videoUrl = metaData?.downloadURL()?.absoluteString {
                print("Down load url is:", videoUrl)
                self.storeImageInUserImages(image: thumbnailImage, completion: { (imageurl) in
                    self.setVideoImagePropertiesInMessage(imageUrl: imageurl, videoUrl: videoUrl)
                })
                
            }
        }
        videoUploadTask.observe(.progress) { (snapShot) in
            print("Hi Ajay:", snapShot.progress?.completedUnitCount)
            guard let uploadCount = snapShot.progress?.completedUnitCount else { return }
            self.navigationItem.title = String(uploadCount)
        }
    }
    
    //Create an first frame of image from video
    private func thumbnailImageForVideoFileUrl(_ url: URL) -> UIImage?{
        let asset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        do{
            let thumbnailImg = try imageGenerator.copyCGImage(at: CMTimeMake(1, 60), actualTime: nil)
            return UIImage(cgImage: thumbnailImg)
        } catch let error {
            print("Error occured while generating a image frame from Video")
            print("Error is: ", error)
        }
        return nil
    }
    //Compress the image to store in firebase
    private func storeImageInUserImages(image: UIImage, completion: @escaping (String) -> ()) {
        if let compressedImage = UIImageJPEGRepresentation(image, 0.2) {
            let randomImageName = NSUUID().uuidString
            let storageRef = Storage.storage().reference().child("user_images").child("\(randomImageName).jpg")
            storageRef.putData(compressedImage, metadata: nil) { (metaData, error) in
                if let error = error {
                    print("error occured while uploading the image into storage", error)
                    return
                }
                if let imgUrl = metaData?.downloadURL()?.absoluteString {
                    completion(imgUrl)
                }
            }
        }
        
    }
    
    private func setImagePropertiesInMessage(imageUrl: String) {
        let properties: [String: AnyObject] = ["imageURL": imageUrl as AnyObject]
        insertImageUrlInMessage(properties: properties)
    }
    private func setVideoImagePropertiesInMessage(imageUrl: String, videoUrl: String) {
        let properties: [String: AnyObject] = ["imageURL": imageUrl as AnyObject, "videoURL": videoUrl as AnyObject]
        insertImageUrlInMessage(properties: properties)
    }
    
    private func insertImageUrlInMessage(properties: [String: AnyObject]) {
        textField.resignFirstResponder()
        let ref = Database.database().reference().child("messages")
        let childRef = ref.childByAutoId()
        guard let fromId = Auth.auth().currentUser?.uid else {
            print("UID is nil in handleSendEvent()")
            return
        }
        guard let receiverID = user?.userID else {
            print("Receiver id is nil in handleSendEvent()")
            return
        }
        let timestamp: NSNumber = NSNumber(value: Date().timeIntervalSince1970)
        var values: [String: AnyObject] = ["fromId": fromId, "toId": receiverID, "timestamp": timestamp] as [String : AnyObject]
        properties.forEach{values[$0.key] = $0.value}
        childRef.updateChildValues(values) { (error, ref) in
            if error != nil {
                print("errror occured while sending the message in handleSendEvent() ", error ?? "Error body is NA")
                return
            }
            let userMessagesRef = Database.database().reference().child("user_messages")
            //Update message under from user . toUser
            let fromRef = userMessagesRef.child(fromId).child(receiverID)
            fromRef.updateChildValues([childRef.key:1])
            if let toUID = self.user?.userID {
                let toRef = userMessagesRef.child(toUID).child(fromId)
                //Update the message under to user
                toRef.updateChildValues([childRef.key:1])
            }
        }
        
        DispatchQueue.main.async {
            //Don't forget to get into the main thread
        }
    }
}


/*************************************************************************************************
 *
 *
 *
 ************************************************************************************************/
//PRAGMA MARK:- Constraints extension
extension ChatLogViewController {
    //Add constraints
    func addConstraints() {
        //Don't have to set containerView constraints, if we us inputAccessoryView. but have to declare the frame. We declared it in containerView initialization
        //        self.containerView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        //        self.containerView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        //        self.containerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        //        containerViewBottomConstraint = self.containerView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
        //        containerViewBottomConstraint?.isActive = true
        
        sendButton.rightAnchor.constraint(equalTo: containerView.rightAnchor).isActive = true
        sendButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
        sendButton.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        sendButton.widthAnchor.constraint(equalToConstant: 80).isActive = true
        
        uploadImageView.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 8).isActive = true
        uploadImageView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
        uploadImageView.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        uploadImageView.widthAnchor.constraint(equalToConstant: 40).isActive = true
        
        textField.leftAnchor.constraint(equalTo: uploadImageView.rightAnchor, constant: 8).isActive = true
        textField.rightAnchor.constraint(equalTo: sendButton.leftAnchor).isActive = true
        textField.heightAnchor.constraint(equalTo:containerView.heightAnchor).isActive = true
        textField.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        
        separatorLineView.leftAnchor.constraint(equalTo: containerView.leftAnchor).isActive = true
        separatorLineView.rightAnchor.constraint(equalTo: containerView.rightAnchor).isActive = true
        separatorLineView.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        separatorLineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        messageCollectionView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        messageCollectionView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        messageCollectionView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 20).isActive = true
        messageCVBottomConstraint = messageCollectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -containerViewHeight)
        messageCVBottomConstraint?.isActive = true
    }
}
