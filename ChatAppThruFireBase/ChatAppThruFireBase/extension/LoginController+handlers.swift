//
//  LoginController+handlers.swift
//  ChatAppThruFireBase
//
//  Created by Naga Murala on 1/27/18.
//  Copyright © 2018 Naga Murala. All rights reserved.
//

//This page has to be expired.. LoginViewcontroller+handlers is the replacement of this class
import UIKit
import Firebase

extension LoginController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @objc func handleSelectProfileImage() {
        let picker = UIImagePickerController()
        //Assign delegate to self to override picker delegate methods
        picker.delegate = self
        //This will allow member to edit the selected image in camara role
        picker.allowsEditing = true
        present(picker, animated: true, completion: nil)
    }
    
    func handleRegeistration() {
        guard let email = emailView.text,let password = passwordView.text, let name = userNameView.text else { return }
        Auth.auth().createUser(withEmail: email, password: password) { [unowned self](user, error) in
            if error != nil {
                print("Something went wrong with fire base email Authentication:", error?.localizedDescription as Any)
                return
            }
            guard let user = user else { return }
            let imageName = NSUUID().uuidString
            //Upload the image into storage
            let storageRef = Storage.storage().reference().child("profile_images").child("\(imageName).jpg")
            //            if let uploadData = UIImagePNGRepresentation(self.profileImageView.image!)
            //Becareful with force unwrapping. Try safely unwrap it in future
            if let uploadData = UIImageJPEGRepresentation(self.profileImageView.image!, 0.1)
            {
                storageRef.putData(uploadData, metadata: nil, completion: { [unowned self](metaData, error) in
                    if error != nil {
                        print(error ?? "Error while insertng image into Storage")
                        return
                    }
                    //Fetch the image uploaded url from storage and pass it to database to store along with user input values.
                    if let profileImageUrl = metaData?.downloadURL()?.absoluteString {
                        let userValues = ["email":email, "name":name, "profileImageURL": profileImageUrl]
                        self.registerUserIntoFireBaseWithUID(uid: user.uid, values: userValues)
                    }
                })
            }
        }
        
    }
    
    func handleLogin() {
        //implement the login logic
        guard let email = emailView.text, let password = passwordView.text else { return }
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if error != nil  {
                print("Unable to sign in with the credentials:", error?.localizedDescription ?? "nil")
                let alert = UIAlertController(title: "Login Failed", message: "Invalid credentials, Please check and try login again", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
            print("User signed in successfully")
            NotificationCenter.default.post(name: NSNotification.Name("Notification_load_CurrenUser"), object: nil, userInfo: nil)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func registerUserIntoFireBaseWithUID(uid: String, values: [String: String]) {
        let ref = Database.database().reference(fromURL: "https://chatappthrufirebase.firebaseio.com/")
        let mainChild = ref.child("users")
        let userChild = mainChild.child(uid)
        
        userChild.updateChildValues(values, withCompletionBlock: { (error, ref) in
            if error != nil {
                print("Something went wront while inserting values into Firebase:", error?.localizedDescription ?? "Error is nil")
                return
            }
            print("Firebase has been updated successfully")
            let user = User(json: values as [String : AnyObject])
            let userDict = ["user" : user]
            NotificationCenter.default.post(name: NSNotification.Name("Notification_load_CurrenUser"), object: nil, userInfo: userDict)
            self.dismiss(animated: true, completion: nil)
        })
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let cropImage = info["UIImagePickerControllerEditedImage"] {
            profileImageView.image = cropImage as? UIImage
        } else if let originalImage = info["UIImagePickerControllerOriginalImage"] {
            profileImageView.image = originalImage as? UIImage
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
