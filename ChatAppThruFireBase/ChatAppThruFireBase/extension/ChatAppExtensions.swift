//
//  ChatAppExtensions.swift
//  ChatAppThruFireBase
//
//  Created by Naga Murala on 1/15/18.
//  Copyright © 2018 Naga Murala. All rights reserved.
//

import UIKit

extension UIColor {
    //Convenience initializer to customize the UIColor
    public convenience init(r: CGFloat, g:CGFloat, b: CGFloat) {
        self.init(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
}
