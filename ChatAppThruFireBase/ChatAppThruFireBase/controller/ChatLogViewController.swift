//
//  ChatLogViewController.swift
//  ChatAppThruFireBase
//
//  Created by Naga Murala on 5/1/18.
//  Copyright © 2018 Naga Murala. All rights reserved.
//

import ProjectWorldFramework
import Firebase

class ChatLogViewController: MasterViewController, MMCollectionItemDelegate {
    
    var user: User?
    var messages: [Message] = [Message]()
    let messageCollectionView: ChatMessageCollectionView = {
        var cv = ChatMessageCollectionView(frame: CGRect.zero)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor(red: 0.95, green: 0.96, blue: 0.96, alpha: 1.0)
        return cv
    }()
    lazy var containerView: UIView = {
        let containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.frame = CGRect(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: containerViewHeight)
        containerView.backgroundColor = .white
        return containerView
    }()
    
    let sendButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Send", for: .normal)
        button.addTarget(self, action: #selector(handleSendEvent), for: .touchUpInside)
        return button
    }()
    let textField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.placeholder = "Enter the Message"
        return textField
    }()
    lazy var uploadImageView: UIImageView = {
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 100, height: 300))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = #imageLiteral(resourceName: "Upload-icon")
        imageView.isUserInteractionEnabled = true
        imageView.contentMode = .scaleAspectFill
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleUploadImageViewEvent))
        imageView.addGestureRecognizer(tapGesture)
        return imageView
    }()
    
    let separatorLineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(r: 220, g: 220, b: 220)
        return view
    }()
    
    var messageCVBottomConstraint: NSLayoutConstraint?
    let containerViewHeight: CGFloat = 50.0
    
    
    //MARK:- View controller Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationTitleView()
        configurations()
        addKeyboardNotifications()
        addSubViews()
        addConstraints()
        getAllMessages()
        self.view.backgroundColor = UIColor(red: 0.95, green: 0.96, blue: 0.96, alpha: 1.0)
        self.messageCollectionView.collectionItemDelegate = self
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        self.messageCollectionView.layout?.invalidateLayout()
        self.messageCollectionView.layoutIfNeeded()
    }
    
    private func setNavigationTitleView() {
        let titleView = NavigationTitleView()
        titleView.nameLabel.text = user?.name ?? "New Chat"
        if let profileImageURL = user?.profileImageURL {
            titleView.profileImageView.loadImageUsingCacheWithUrlString(urlString: profileImageURL)
        }
        self.navigationItem.titleView = titleView
    }
    
    private func configurations() {
        self.textField.delegate = self
        self.messageCollectionView.keyboardDismissMode = .interactive
        self.messageCollectionView.alwaysBounceVertical = true
    }
    
    private func addKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(adjustHeightWithKeyBoard), name: Notification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(adjustHeightWithOutKeyBoard), name: Notification.Name.UIKeyboardWillHide, object: nil)
    }
    
    private func addSubViews() {
        //Don't have to add the subview. inputAccessoryView will take care of it.
//        self.view.addSubview(containerView)
        //Container view subviews
        containerView.addSubview(textField)
        containerView.addSubview(sendButton)
        containerView.addSubview(separatorLineView)
        containerView.addSubview(uploadImageView)
        self.view.addSubview(messageCollectionView)
    }
    //MasterItem delegate methods
    var startingFrame: CGRect?
    var blackBackgroundView: UIView?
    var startImageView: UIImageView?
    func itemSelected(_ sender: Any, item: Any, itemIdentifier: String?, indexPath: IndexPath) {
        //Disable the zoom feature when user taps on video.
        if let message = item as? Message, message.videoURL != nil {
            return
        }
        guard let cell = self.messageCollectionView.cellForItem(at: indexPath) as? ChatMessageCollectionViewCell else { return }
        startImageView = cell.imageView
        startImageView?.isHidden = true
        guard let startingFrame = startImageView?.superview?.convert((startImageView?.frame)!, to: nil) else { return }
        self.startingFrame = startingFrame
        let zoomingImageView = UIImageView(frame: startingFrame)
        zoomingImageView.image = startImageView?.image
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(zoomOutGesture))
        zoomingImageView.addGestureRecognizer(tapGesture)
        zoomingImageView.isUserInteractionEnabled = true
        guard let keyWindow = UIApplication.shared.keyWindow else { return }
        blackBackgroundView = UIView(frame: keyWindow.frame)
        blackBackgroundView?.backgroundColor = .black
        blackBackgroundView?.alpha = 0
        blackBackgroundView?.addSubview(zoomingImageView)
        keyWindow.addSubview(blackBackgroundView!)
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn, animations: {
            self.blackBackgroundView?.alpha = 1
            self.containerView.alpha = 0
            let height = startingFrame.height / startingFrame.width * keyWindow.frame.width
            zoomingImageView.frame = CGRect(x: 0, y: 0, width: keyWindow.frame.width, height: height)
            zoomingImageView.center = keyWindow.center
        }, completion: nil)
    }
    
    @objc func zoomOutGesture(tapGesture: UITapGestureRecognizer) {
        let view = tapGesture.view
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            view?.frame = self.startingFrame!
            self.blackBackgroundView?.alpha = 0
            self.containerView.alpha = 1
            self.startImageView?.isHidden = false
        }) { (isCompleted) in
            self.blackBackgroundView?.removeFromSuperview()
        }
    }
    //Remove all the notification when current object is going to die
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardWillHide, object: nil)
    }
}


