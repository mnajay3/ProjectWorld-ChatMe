//
//  LoginController.swift
//  ChatAppThruFireBase
//
//  Created by Naga Murala on 1/15/18.
//  Copyright © 2018 Naga Murala. All rights reserved.
//

import UIKit
import Firebase


//Have to migrate all these to LoginViewController eventually
class LoginController: UIViewController, UITextFieldDelegate{
    //PAGMARK:- UI Element Initializaion
    lazy var profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "winteriscoming").withRenderingMode(.alwaysOriginal)
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleSelectProfileImage))
        imageView.addGestureRecognizer(tapGesture)
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
   lazy var loginRegisterSegmentControl: UISegmentedControl = {
       let sc = UISegmentedControl(items: ["Login", "Register"])
        sc.translatesAutoresizingMaskIntoConstraints = false
        sc.tintColor = .white
        sc.selectedSegmentIndex = 0
        self.perform(#selector(handleLoginRegisterSegmentControl), with: nil, afterDelay: 0)
        sc.addTarget(self, action: #selector(handleLoginRegisterSegmentControl), for: .valueChanged)
        return sc
    }()
    var loginContainerView: UIView = {
        let lv = UIView()
        lv.translatesAutoresizingMaskIntoConstraints = false
        lv.backgroundColor = .white
        lv.layer.cornerRadius = 10
        lv.layer.masksToBounds = true
        return lv
    }()
    var loginRegisterButton : UIButton = {
        let lrb = UIButton(type: .system)
        lrb.translatesAutoresizingMaskIntoConstraints = false
        lrb.backgroundColor = UIColor(r: 80, g: 101, b: 161)
        lrb.setTitle("Register", for: .normal)
        lrb.setTitleColor(.white, for: .normal)
        lrb.addTarget(self, action: #selector(handleLoginRegistrationButton), for: .touchUpInside)
        return lrb
    }()
    lazy var userNameView: UITextField = {
        let unv = UITextField()
        unv.translatesAutoresizingMaskIntoConstraints = false
        unv.placeholder = "UserName"
        unv.layer.cornerRadius = 10
        unv.delegate = self
        return unv
    }()
    lazy var emailView: UITextField = {
        let eV = UITextField()
        eV.translatesAutoresizingMaskIntoConstraints = false
        eV.placeholder = "Email"
        eV.layer.cornerRadius = 10
        eV.keyboardType = .emailAddress
        eV.delegate = self
        return eV
    }()
    lazy var passwordView: UITextField = {
        let pwv = UITextField()
        pwv.translatesAutoresizingMaskIntoConstraints = false
        pwv.placeholder = "Password"
        pwv.layer.cornerRadius = 10
        pwv.isSecureTextEntry = true
        pwv.delegate = self
        return pwv
    }()
    var borderView: UIView = {
        let bv = UIView()
        bv.translatesAutoresizingMaskIntoConstraints = false
        bv.backgroundColor = .lightGray
        return bv
    }()
    var emailBorderView: UIView = {
        let bv = UIView()
        bv.translatesAutoresizingMaskIntoConstraints = false
        bv.backgroundColor = .lightGray
        return bv
    }()
    
    //PAGMARK:- ViewController overriden method
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(r: 61, g: 91, b: 151)
        //Set the subivews
        setupViews()
    }
    
    //Method is used to handle the device orientation.
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        switch UIDevice.current.orientation {
        case .portrait:
            profileViewHeightConstraint?.constant = 150
        case .portraitUpsideDown:
            profileViewHeightConstraint?.constant = 150
        case .landscapeLeft:
            profileViewHeightConstraint?.constant  = 75
        case .landscapeRight:
            profileViewHeightConstraint?.constant = 75
        default:
            break
        }
    }
    
    //PAGMARK:- UIApplication Overriden Method
    //To display the statusbar text color as white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    //PAGMARK:- SetupView - Setup VIEW HEIRARCHY
    func setupViews() {
        view.addSubview(profileImageView)
        view.addSubview(loginRegisterSegmentControl)
        view.addSubview(loginContainerView)
        loginContainerView.addSubview(userNameView)
        loginContainerView.addSubview(borderView)
        loginContainerView.addSubview(emailView)
        loginContainerView.addSubview(emailBorderView)
        loginContainerView.addSubview(passwordView)
        view.addSubview(loginRegisterButton)
        //Setup the constraints. Make sure to add constraints after the adding the view in heirarchy
        addConstraints()
    }
    //PAGMARK:- Event Handle methods from UIButtons
    //Handle the event from segment control
    @objc func handleLoginRegisterSegmentControl() {
        let index = loginRegisterSegmentControl.selectedSegmentIndex
        let title = loginRegisterSegmentControl.titleForSegment(at: index)
        loginRegisterButton.setTitle(title, for: .normal)
        if index == 0 {
            userNameViewConstraint?.isActive = false
            userNameViewConstraint = userNameView.heightAnchor.constraint(equalTo: loginContainerView.heightAnchor, multiplier: 0)
            userNameViewConstraint?.isActive = true
            
            emailViewConstraint?.isActive = false
            emailViewConstraint = emailView.heightAnchor.constraint(equalTo: loginContainerView.heightAnchor, multiplier: 1/2)
            emailViewConstraint?.isActive = true
            
            passwordViewConstraint?.isActive = false
            passwordViewConstraint = passwordView.heightAnchor.constraint(equalTo: loginContainerView.heightAnchor, multiplier: 1/2)
            passwordViewConstraint?.isActive = true
            profileImageView.isUserInteractionEnabled = false
        } else {
            userNameViewConstraint?.isActive = false
            userNameViewConstraint = userNameView.heightAnchor.constraint(equalTo: loginContainerView.heightAnchor, multiplier: 1/3)
            userNameViewConstraint?.isActive = true
            
            emailViewConstraint?.isActive = false
            emailViewConstraint = emailView.heightAnchor.constraint(equalTo: loginContainerView.heightAnchor, multiplier: 1/3)
            emailViewConstraint?.isActive = true
            
            passwordViewConstraint?.isActive = false
            passwordViewConstraint = passwordView.heightAnchor.constraint(equalTo: loginContainerView.heightAnchor, multiplier: 1/3)
            passwordViewConstraint?.isActive = true
            profileImageView.isUserInteractionEnabled = true
        }
        loginContainerViewHeightConstraint?.constant = index == 0 ? 100 : 150
        

    }
    //Method to handle event from loginRegistrationbutton
    @objc func handleLoginRegistrationButton() {
        let index = loginRegisterSegmentControl.selectedSegmentIndex
        switch index {
        case 0:
            handleLogin()
        case 1:
            handleRegeistration()
        default:
            print("Wrong signal received from segmentControl", index)
        }
    }
    
    
    //PAGMARK:- Add Constraints Programatically
    var profileViewHeightConstraint: NSLayoutConstraint?
    var userNameViewConstraint: NSLayoutConstraint?
    var emailViewConstraint: NSLayoutConstraint?
    var passwordViewConstraint: NSLayoutConstraint?
    var loginContainerViewHeightConstraint: NSLayoutConstraint?
    func addConstraints() {
        //profileImageView:top,y, height, width
        profileImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        profileImageView.bottomAnchor.constraint(equalTo: loginRegisterSegmentControl.topAnchor, constant: -20).isActive = true
        profileViewHeightConstraint = NSLayoutConstraint(item: profileImageView, attribute: .height, relatedBy: .equal, toItem: self.view, attribute: .height, multiplier: 0, constant: 150)
        view.addConstraint(profileViewHeightConstraint!)
        profileImageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        // segment Control: x,y,bottom,height,width
        loginRegisterSegmentControl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loginRegisterSegmentControl.heightAnchor.constraint(equalToConstant: 32).isActive = true
        loginRegisterSegmentControl.widthAnchor.constraint(equalTo: loginContainerView.widthAnchor, multiplier: 1).isActive = true
        loginRegisterSegmentControl.bottomAnchor.constraint(equalTo: loginContainerView.topAnchor, constant: -10).isActive = true
        //loginContainerView: left,right,height, top
        loginContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loginContainerView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        loginContainerViewHeightConstraint = loginContainerView.heightAnchor.constraint(equalToConstant: 150)
        loginContainerViewHeightConstraint?.isActive = true
        loginContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -10).isActive = true
        //loginRegisterButton: x, y, height, width
        loginRegisterButton.leftAnchor.constraint(equalTo: loginContainerView.leftAnchor).isActive = true
        loginRegisterButton.rightAnchor.constraint(equalTo: loginContainerView.rightAnchor).isActive = true
        loginRegisterButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        loginRegisterButton.topAnchor.constraint(equalTo: loginContainerView.bottomAnchor, constant: 12).isActive = true
        //userNameView: left, right, top, height
        userNameView.leftAnchor.constraint(equalTo: loginContainerView.leftAnchor, constant: 12).isActive = true
        userNameView.rightAnchor.constraint(equalTo: loginContainerView.rightAnchor).isActive = true
        userNameView.topAnchor.constraint(equalTo: loginContainerView.topAnchor).isActive = true
        userNameViewConstraint = userNameView.heightAnchor.constraint(equalTo: loginContainerView.heightAnchor, multiplier: 1/3)
        userNameViewConstraint?.isActive = true
        //borderView
        borderView.leftAnchor.constraint(equalTo: loginContainerView.leftAnchor).isActive = true
        borderView.rightAnchor.constraint(equalTo: loginContainerView.rightAnchor).isActive = true
        borderView.topAnchor.constraint(equalTo: userNameView.bottomAnchor, constant: -1).isActive = true
        borderView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        //emailView:
        emailView.leftAnchor.constraint(equalTo: loginContainerView.leftAnchor, constant: 12).isActive = true
        emailView.rightAnchor.constraint(equalTo: loginContainerView.rightAnchor).isActive = true
        emailView.topAnchor.constraint(equalTo: userNameView.bottomAnchor).isActive = true
        emailViewConstraint = emailView.heightAnchor.constraint(equalTo: loginContainerView.heightAnchor, multiplier: 1/3)
        emailViewConstraint?.isActive = true
        //borderView
        emailBorderView.leftAnchor.constraint(equalTo: loginContainerView.leftAnchor).isActive = true
        emailBorderView.rightAnchor.constraint(equalTo: loginContainerView.rightAnchor).isActive = true
        emailBorderView.topAnchor.constraint(equalTo: emailView.bottomAnchor, constant: -1).isActive = true
        emailBorderView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        //passwordView
        passwordView.leftAnchor.constraint(equalTo: loginContainerView.leftAnchor, constant: 12).isActive = true
        passwordView.rightAnchor.constraint(equalTo: loginContainerView.rightAnchor).isActive = true
        passwordView.topAnchor.constraint(equalTo: emailView.bottomAnchor).isActive = true
        passwordViewConstraint = passwordView.heightAnchor.constraint(equalTo: loginContainerView.heightAnchor, multiplier: 1/3)
        passwordViewConstraint?.isActive = true
    }
    
    //PAGMARK:- UITextField Overriden Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //Call the following to resign the keyboard when user hit's return key
        view.endEditing(true)
        return true
    }
    
}
