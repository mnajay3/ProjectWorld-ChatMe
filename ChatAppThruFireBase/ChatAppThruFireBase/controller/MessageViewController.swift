//
//  ViewController.swift
//  ChatAppThruFireBase
//
//  Created by Naga Murala on 1/15/18.
//  Copyright © 2018 Naga Murala. All rights reserved.
//

import UIKit
import Firebase
import ProjectWorldFramework

class MessageViewController: MasterViewController,MasterItemDelegate, ContactViewDelegate {
    lazy var messagesTableview: MessagesTableView = {
        var frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        let tv = MessagesTableView(frame: frame, style: .plain)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.tableFooterView = UIView()
        return tv
    }()
    
    var users:[User] = [User]()
    var messages:[Message] = [Message]()
    var messageDictionary:[String: Message] = [String: Message]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(messagesTableview)
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(handleLogout))
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "createNew"), style: .plain, target: self, action: #selector(handleCreateNew))
        checkUserLoginStatus()
        //Add the observer to load the logged in user details on the navigation bar
        NotificationCenter.default.addObserver(self, selector: #selector(handleNotificationFromLogin(notification:)), name: NSNotification.Name("Notification_load_CurrenUser"), object: nil)
        self.messagesTableview.itemDelegate = self
        //        observeMessages()
        addConstraints()
    }
    
    func addConstraints() {
        messagesTableview.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        messagesTableview.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        messagesTableview.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        messagesTableview.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
    }
    
    /**
     This method initially triggers when the user login in to the app. It fetches all user messages from the database and renders it into the UI.
     In addition, It always be active to listent to when the new messages will be added. It observes for the childAdded event to be raised.
     and even it listens to child deleted in case of user deletes the messages row. so that it deletes the toID from "user_messages", when ever this delete happens. it listen to it and reloads all the messages
 
 
 **/
    private func observeUserMessages() {
        //Empty messages evertime member logout and login again
        self.messages = [Message]()
        self.messageDictionary = [String: Message]()
        self.messagesTableview.list = [0: self.messages]
        self.messagesTableview.reloadData()
        
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        let userMsgsRef = Database.database().reference().child("user_messages").child(uid)
        userMsgsRef.observe(.childAdded, with: { (snapShot) in
            /** new heirarchy of user_message changes starts **/
            let user_messages_uid = snapShot.key
            print("user_messages_uid", user_messages_uid)
            Database.database().reference().child("user_messages").child(uid).child(user_messages_uid).observe(.childAdded, with: { (snapShot) in
                let messageID = snapShot.key
                print("messageID", messageID)
                let msgRef = Database.database().reference().child("messages").child(messageID)
                msgRef.observeSingleEvent(of: .value, with: { (snapShot) in
                    if let dictionary = snapShot.value as? [String : Any] {
                        let message = Message(json: dictionary)
                        if let toID = message.toId, let fromID = message.fromId {
                            if uid == fromID {
                                self.messageDictionary[toID] = message
                            } else if uid == toID {
                                self.messageDictionary[fromID] = message
                            }
                            self.messages = Array(self.messageDictionary.values)
                            print("messages", self.messages)
                            //Sorting the messages array to display the messages in order in messageviewcontroller
                            self.messages.sort(by: { (message1, message2) -> Bool in
                                guard let time1 = message1.timestamp?.doubleValue else { return false }
                                guard let time2 = message2.timestamp?.doubleValue else { return true }
                                return time1 > time2
                            })
                        }
                        //This is so bad, we are pausing the main thread for 0.5 seconds until all background threads are completed. We shouldn't be doing this.
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                            print("messages main", self.messages)
                            self.messagesTableview.list = [0: self.messages]
                            self.messagesTableview.reloadData()
                        })
                    }
                    
                }, withCancel: nil)
            }, withCancel: nil)
            /** changes end **/
        }, withCancel: nil)
        
        //If user swiped left and click on delete on a message row. this evenlistener observes for it and deletes the deleted row from messageDictionary and reloads the table data.
        userMsgsRef.observe(.childRemoved, with: { (snapShot) in
            self.messageDictionary.removeValue(forKey: snapShot.key)
            self.messages = Array(self.messageDictionary.values)
            self.messagesTableview.list = [0: self.messages]
            self.messagesTableview.reloadData()
        }, withCancel: nil)
    }
    
    @objc func handleNotificationFromLogin(notification: Notification) {
        checkUserLoginStatus()
        guard let user = notification.userInfo?["user"] else { return }
        setNavigationTitleView(user: user as! User)
    }
    
    //Check the user login status when initial load of the app
    func checkUserLoginStatus() {
        //User is not logged in
        if Auth.auth().currentUser?.uid == nil {
            perform(#selector(handleLogout), with: nil, afterDelay: 0)
        }else {
            guard let uid = Auth.auth().currentUser?.uid else { return }
            Database.database().reference().child("users").child(uid).observeSingleEvent(of: .value, with: { [unowned self](snapshot) in
                guard let userJson = snapshot.value as? [String : AnyObject] else {
                    //If the user got deleted from Firebase, we are forcing member to redirect to the login page
                    self.perform(#selector(self.handleLogout), with: nil, afterDelay: 0)
                    print("User is null")
                    return
                }
                let user = User(json: userJson)
                self.setNavigationTitleView(user: user)
                DispatchQueue.main.async {
                    //                    self.observeMessages()
                    self.observeUserMessages()
                }
            })
        }
    }
    //Handle logout left bar button event
    @objc func handleLogout() {
        do {
            try Auth.auth().signOut()
        } catch let signOutErr {
            print("Unable to signout the user:", signOutErr)
        }
//        let logingVC = LoginController()
        let logingVC = LoginViewController()
        present(logingVC, animated: true, completion: nil)
    }
    //EventDelegation method to handle the event of Create new right navigtion bar button
    @objc func handleCreateNew() {
        let contactsVC = ContactsViewController()
        contactsVC.contactDelegate = self
        let navController = UINavigationController(rootViewController: contactsVC)
        self.present(navController, animated: true, completion: nil)
    }
    //Set the navigation title along with profile image
    func setNavigationTitleView(user: User) {
        //Reusing the NavigationTitleView class to display image along with the title in navigation bar
        let titleView = NavigationTitleView(frame: CGRect(x: 0, y: 0, width: 100, height: 40))
        if let profileImageURL = user.profileImageURL {
            titleView.profileImageView.loadImageUsingCacheWithUrlString(urlString: profileImageURL)
        }
        if let title = user.name {
            titleView.nameLabel.text = title
        }
        self.navigationItem.titleView = titleView
    }
    
    //Call chat view controller, when user click on any contact in contact list
    func contactSelected(_ sender: Any, item: Any, itemIdentifier: String?, indexPath: IndexPath) {
        let chatVC = ChatLogViewController()
        chatVC.user = item as? User ?? nil
        self.navigationController?.pushViewController(chatVC, animated: true)
    }
    
    //ItemDelegate method for MessageView Controller
    func itemSelected(_ sender: Any, item: Any, itemIdentifier: String?, indexPath: IndexPath) {
        var item = item
        //The reason to assing user for item is ChatViewController always expects the item as user to display navigation bar title. In future this requirement may change.
        if let user = ((item as? Message)?.toUser) {
            item = user
        }
        contactSelected(sender, item: item, itemIdentifier: itemIdentifier, indexPath: indexPath)
    }
    //Remove all the notifications in de initialization process
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("Notification_load_CurrenUser"), object: nil)
    }
}

