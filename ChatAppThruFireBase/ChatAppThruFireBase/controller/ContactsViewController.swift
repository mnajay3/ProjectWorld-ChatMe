//
//  ContactsViewController.swift
//  ChatAppThruFireBase
//
//  Created by Naga Murala on 1/23/18.
//  Copyright © 2018 Naga Murala. All rights reserved.
//

import UIKit
import ProjectWorldFramework
import Firebase

protocol ContactViewDelegate {
    func contactSelected(_ sender: Any, item: Any, itemIdentifier: String?, indexPath: IndexPath)
}

class ContactsViewController: MasterViewController, MasterItemDelegate {
    
    var users: [User] = [User]()
    var temp: [String] = [String]()
    var contactDelegate: ContactViewDelegate?
    
    lazy var contactsTableView: ContactsTableView = {
        let frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        let tv = ContactsTableView(frame: frame, style: .plain)
        tv.translatesAutoresizingMaskIntoConstraints = false
        //Set the footerView as empty view to hide empty rows in a table
        tv.tableFooterView = UIView()
        return tv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(contactsTableView)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(handleBack))
        self.navigationItem.title = "Contacts"
        users = [User]()
        fetchUsers()
        self.contactsTableView.itemDelegate = self
        addConstraints()
    }
    
    @objc func handleBack() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func fetchUsers() {
        //To fetch all the users in FireBase
//        Database.database().reference().child("users").observe(.childAdded, with: { (snapshot) in
//            guard let dictionary = snapshot.value as? [String : AnyObject] else { return }
//            let user = User(json: dictionary)
//            self.users.append(user)
//        }, withCancel: nil)
        
        /**
         users
          -- UID
             -- name
             -- email
         First statement we will get the complete snapshot
         we will iterate by getting all the children from snapshot( consists of all uid's)
         each item is email and name under the uid.. so we will get values and assign it to model object
 **/
        Database.database().reference().child("users").observe(.value, with: { (snapshot) in
            for item in snapshot.children {
                guard let userSnapshot = item as? DataSnapshot else { return }
                let userKey = userSnapshot.key
                guard let userDict = userSnapshot.value as? [String : AnyObject] else { return }
                let user = User(json: userDict, withKey : userKey)
                self.users.append(user)
            }
            self.contactsTableView.list = [0:self.users]
            //Observed this is running in main thread but don't know just declared manually to run on mainthread again.. have to look more on it.
            DispatchQueue.main.async {
                self.contactsTableView.reloadData()
            }
        }, withCancel: nil)
    }
    
    //MARK:- MasterItemDelegate overriden methods
    func itemSelected(_ sender: Any, item: Any, itemIdentifier: String?, indexPath: IndexPath) {
        dismiss(animated: true) {
            self.contactDelegate?.contactSelected(self, item: item, itemIdentifier: itemIdentifier, indexPath: indexPath)
        }
    }
    
    func addConstraints() {
        contactsTableView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        contactsTableView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        contactsTableView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        contactsTableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
    }
}
