//
//  LoginViewController.swift
//  audible
//
//  Created by Naga Murala on 7/17/18.
//  Copyright © 2018 Naga Murala. All rights reserved.
//

import UIKit
import Firebase

//#MARK:- It is a new controller.. Under migration

class LoginViewController: UIViewController, UITextFieldDelegate {
    var pageControlBottomAnchor: NSLayoutConstraint?
    var skipTopAnchor: NSLayoutConstraint?
    var nextTopAnchor: NSLayoutConstraint?
    var cell: AnyObject?
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        let cv  = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.isPagingEnabled = true
        cv.backgroundColor = .white
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    
    //to hide status bar
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    lazy var pageControl: UIPageControl = {
        let pc = UIPageControl()
        pc.pageIndicatorTintColor = .lightGray
        pc.currentPageIndicatorTintColor = UIColor(red: 247/255, green: 154/255, blue: 27/255, alpha: 1)
        pc.numberOfPages = page.count + 1
        return pc
    }()
    
    //#MARK:- Skip button stuff
    var skipButton: UIButton = {
        let sb = UIButton(type: .system)
        sb.setTitle("Skip", for: .normal)
        sb.setTitleColor(UIColor(red: 247/255, green: 154/255, blue: 27/255, alpha: 1), for: .normal)
        sb.addTarget(self, action: #selector(skip), for: .touchUpInside)
        return sb
    }()
    
    @objc func skip() {
        //Setting the current page as previous to the login page and calling nextPage() method to goto login page
        pageControl.currentPage = page.count - 1
        nextPage()
    }
    
    //#MARK:- Next button stuff
    lazy var nextButton: UIButton = {
        let nb = UIButton(type: .system)
        nb.setTitle("Next", for: .normal)
        nb.setTitleColor(UIColor(red: 247/255, green: 154/255, blue: 27/255, alpha: 1), for: .normal)
        nb.addTarget(self, action: #selector(nextPage), for: .touchUpInside)
        return nb
    }()
    
    @objc func nextPage(){
        //Hide next button and skip button if the page is last button page and next button clicked
        if pageControl.currentPage == page.count - 1 {
            hidePageControlAndButtons()
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                //Super Mandatory method to show animation when constraints are changed
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
        let indexPath = IndexPath(item: pageControl.currentPage + 1, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        pageControl.currentPage += 1
        
    }
    
    //#MARK:-
    lazy var page: [Page] = {
        let page1 = Page(title: "Share a Great Listen", message: "It's free to send the books to the people in your life. Every recipient's first book on us.", image: #imageLiteral(resourceName: "page3"))
        let page7 = Page(title: "Share a Great Listen", message: "It's free to send the books to the people in your life. Every recipient's first book on us.", image: #imageLiteral(resourceName: "page7"))
        let page12 = Page(title: "Share a Great Listen", message: "It's free to send the books to the people in your life. Every recipient's first book on us.", image: #imageLiteral(resourceName: "page12"))
        let page16 = Page(title: "Share a Great Listen", message: "It's free to send the books to the people in your life. Every recipient's first book on us.", image: #imageLiteral(resourceName: "page16"))
        let page17 = Page(title: "Share a Great Listen", message: "It's free to send the books to the people in your life. Every recipient's first book on us.", image: #imageLiteral(resourceName: "page17"))
        let page18 = Page(title: "Share a Great Listen", message: "It's free to send the books to the people in your life. Every recipient's first book on us.", image: #imageLiteral(resourceName: "page18"))
        return [page1, page7, page12, page16, page17, page18]
    }()
    
    let cellID = "cellID"
    let loginCellID = "loginCellID"
    
    //#MARK:- LoginViewController - Overloaded methods
    override func viewWillLayoutSubviews() {
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(collectionView)
        view.addSubview(pageControl)
        view.addSubview(skipButton)
        view.addSubview(nextButton)
        setupViews()
        registerCollectionViewCells()
        observeKeyboardNotifications()
    }
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        let indexPath = IndexPath(item: pageControl.currentPage, section: 0)
        DispatchQueue.main.async {
            self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }
        self.collectionView.reloadData()
    }
    
    //#MARK:- View Constraints
    func setupViews() {
        //Set the constraints using UIView extensions method
        collectionView.setConstraints(top: view.topAnchor, bottom: view.bottomAnchor, left: view.leftAnchor, right: view.rightAnchor)
        pageControlBottomAnchor = pageControl.anchor(top: nil, bottom: view.bottomAnchor, left: view.leftAnchor, right: view.rightAnchor, topConstant: 0, bottomConstant: 0, leftConstant: 0, rightConstant: 0, height: 40, width: 0)[0]
        skipTopAnchor = skipButton.anchor(top: view.topAnchor, bottom: nil, left: view.leftAnchor, right: nil, topConstant: 15, bottomConstant: 0, leftConstant: 0, rightConstant: 0, height: 50, width: 60)[0]
        nextTopAnchor = nextButton.anchor(top: view.topAnchor, bottom: nil, left: nil, right: view.rightAnchor, topConstant: 15, bottomConstant: 0, leftConstant: 0, rightConstant: 0, height: 50, width: 60)[0]
    }
    
    deinit {
        print("Hi, I am getting deallocated")
    }
}

/****************************************************************************
 
 *    Collection view delegate, data source and flowlayout delegate methods. *
 
 *****************************************************************************/
extension LoginViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    fileprivate func registerCollectionViewCells() {
        collectionView.register(PageCell.self, forCellWithReuseIdentifier: cellID)
        collectionView.register(LoginPageCell.self, forCellWithReuseIdentifier: loginCellID)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // incresed by 1 to display the login page
        return page.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard indexPath.row < page.count else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: loginCellID, for: indexPath) as! LoginPageCell
            cell.loginDelegate = self
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! PageCell
        cell.page = page[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
    
}

/****************************************************************************
 
 *    pagecontrol navigation setup: Using Scroll view override methods       *
 
 *****************************************************************************/
extension LoginViewController {
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        //pageNumber = x point / view width
        let pageNumber = Int(targetContentOffset.pointee.x / self.view.frame.size.width)
        pageControl.currentPage = pageNumber
        if pageNumber == page.count {
            hidePageControlAndButtons()
        } else {
            pageControlBottomAnchor?.constant = 0
            skipTopAnchor?.constant = 15
            nextTopAnchor?.constant = 15
        }
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            //Super Mandatory method to show animation when constraints are changed
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    fileprivate func hidePageControlAndButtons() {
        pageControlBottomAnchor?.constant = 40
        skipTopAnchor?.constant = -40
        nextTopAnchor?.constant = -40
    }
    
    //This method will trigger before the scroll view start editing
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        //Calling end editing to shut off the keyboard
        view.endEditing(true)
    }
    
}

/****************************************************************************
 
 *    Keyboard Hide and show functionalities                                *
 
 *****************************************************************************/
extension LoginViewController {
    fileprivate func observeKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyBoardWillShow() {
        guard let keyFrame = UIApplication.shared.keyWindow?.frame else { return }
        if keyFrame == view.frame {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y - 50, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }, completion: nil)
        }
    }
    
    @objc func keyBoardWillHide() {
        guard let keyFrame = UIApplication.shared.keyWindow?.frame else { return }
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.view.frame = keyFrame
        }, completion: nil)
    }
}

/****************************************************************************
 
 *    LoginPageDelegate: Conforming                                            *
 
 *****************************************************************************/
extension LoginViewController: LoginPageDelegate {
    func handleTapOnProfileImage(_ cell: AnyObject) {
        self.cell = cell
        handleSelectProfileImage()
    }
    
    
    //Method to handle event from loginRegistrationbutton
    func finishLogin(index: Int, cell: AnyObject) {
        switch index {
        case 0:
            handleLogin(cell)
        case 1:
            handleRegeistration(cell)
        default:
            print("Wrong signal received from segmentControl", index)
        }
        dismiss(animated: true) {
            
        }
    }
}
