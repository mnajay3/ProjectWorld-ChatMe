//
//  MasterCollectionViewCell.swift
//  ProjectWorldFramework
//
//  Created by Naga Murala on 11/29/17.
//  Copyright © 2017 Naga Murala. All rights reserved.
//

import UIKit

open class MasterCollectionViewCell: UICollectionViewCell {
    override public init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    open func initialize() {
//        self.backgroundColor = .red
        //NOOP
    }
    
    open func configureCell(collectionView: UICollectionView, item: Any, indexPath: IndexPath ) -> UICollectionViewCell {
        if indexPath.item % 2 == 0 {
            self.backgroundColor = .red
        } else {
            self.backgroundColor = .blue
        }
        return self
    }
    
    
    
    
}
